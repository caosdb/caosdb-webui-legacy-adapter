import React from "react";
import ReactDOM from "react-dom/client";
import { Map, ToggleMapButton } from "@indiscale/caosdb-webui-ext-map";
import { queryCallback } from "./queryCallback";

const caosdb_map_2 = {
  /**
   * Create the map panel (div.caosdb-f-map-panel) and insert it into the dom
   * tree. Then return the map panel.
   */
  init_map_panel: function () {
    // remove old
    if (document.querySelector(".caosdb-f-map-panel")) {
      document.querySelector(".caosdb-f-map-panel").remove();
    }

    const panel = document.createElement("div");
    panel.classList = "caosdb-f-map-panel container mb-2";

    document.querySelector("nav").after(panel);

    return panel;
  },

  /**
   * Create a button item in the navbar and return it.
   *
   * The button contains a dummy link tag which is to be removed/overriden when
   * React inserts the ToggleMapButton component.
   */
  create_navbar_item: function () {
    const dummy = document.createElement("a");
    return navbar.add_button(dummy);
  },

  init: async function () {
    // create map panel
    const panel = this.init_map_panel();

    // insert map into container
    const map_root = ReactDOM.createRoot(panel);
    map_root.render(
      <React.StrictMode>
        <Map queryCallback={queryCallback} />
      </React.StrictMode>,
    );

    const navItem = this.create_navbar_item();

    const button_root = ReactDOM.createRoot(navItem);
    button_root.render(
      <React.StrictMode>
        <ToggleMapButton className="nav-link" />
      </React.StrictMode>,
    );
  },
};

$(document).ready(function () {
  caosdb_modules.register(caosdb_map_2);
});
