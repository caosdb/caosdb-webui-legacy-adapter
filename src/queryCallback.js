const queryCallback = (queryString, pageSize) => {
  const paging = pageSize < 1 ? "" : `&P=0L${pageSize || 10}`;
  const newHref =
    connection.getEntityUri([]) +
    `?query=${encodeURIComponent(queryString)}${paging}`;
  window.location.href = newHref;
};

export { queryCallback };
