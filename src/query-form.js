import React from "react";
import ReactDOM from "react-dom/client";
import {
  QueryForm,
  QueryPanel,
  toggleQueryPanel,
  makeQueryTemplate,
  createTab,
} from "@indiscale/caosdb-webui-core-components";
import { queryCallback } from "./queryCallback";

function queryPanel(
  submitCallback,
  tabs,
  getSuggestionsCallback,
  restore,
  defaultTab,
) {
  const element = document.createElement("DIV");
  document
    .getElementById("top-navbar")
    .querySelector(".caosdb-navbar")
    .after(element);
  element.classList.add("navbar-nav", "caosdb-f-query-panel");

  const scrollHandler = () => {
    toggleQueryPanel(element);
  };

  const root = ReactDOM.createRoot(element);
  root.render(
    <QueryPanel
      scrollHandler={scrollHandler}
      submitCallback={submitCallback}
      tabs={tabs}
      defaultTab={defaultTab}
      getSuggestionsCallback={getSuggestionsCallback}
      restore={restore}
    />,
  );
}

function removeOldQueryPanel() {
  document.getElementById("caosdb-navbar-query").remove();
  document.getElementById("caosdb-query-panel").remove();
}

function resolveDefaultTab(tabs, defaultTab) {
  var result = 0;
  for (let i = 0; i < tabs.length; i++) {
    if (tabs[i].id === defaultTab) {
      result = i;
    }
  }
  return result;
}

/**
 * Loads the config file and initializes the query panel.
 *
 * The config file must validate against the tabs-config.schema.json.
 */
async function initQueryPanel() {
  if (!document.getElementById("caosdb-navbar-query")) {
    // document not ready, retrigger when ready
    document.addEventListener(
      "DOMContentLoaded",
      function () {
        initQueryPanel();
      },
      false,
    );
    return;
  }

  removeOldQueryPanel();

  // for auto completion
  const retrieveNames = async () => {
    var response = $(
      await connection.get(transaction.generateEntitiesUri(["names"])),
    ).find("Property[name],RecordType[name],Record[name]");

    response = response.toArray().map((x) => $(x).attr("name"));

    return response;
  };

  // give function -> lazy, give results -> eager initialization
  const autoCompletion = new QueryForm.AutoCompletion(retrieveNames);
  const getSuggestionsCallback = (isCql, value, word) =>
    autoCompletion.getSuggestions(isCql, value, word);

  // configure tabs
  var tabs_config = await load_config("json/query-form-tabs.json");
  var defaultTab = undefined;
  var prependAllTab = true;

  if (Array.isArray(tabs_config.tabs)) {
    // tabs_config is object { tabs: [...], defaultTab: "...", prependAllTab: false }...
    prependAllTab =
      typeof tabs_config.prependAllTab !== "undefined"
        ? tabs_config.prependAllTab
        : prependAllTab;
    defaultTab = tabs_config.defaultTab;
    tabs_config = tabs_config.tabs;
  } // else { // tabs_config is an array }

  if (tabs_config.map) {
    tabs_config = tabs_config.map((conf) =>
      createTab(
        conf.id,
        conf.recordType,
        conf.label,
        conf.description,
        undefined,
        conf.filterDefinitions,
      ),
    );
    if (prependAllTab && tabs_config.length > 0) {
      tabs_config = [
        createTab(
          "all",
          undefined,
          "All",
          "Show all matching results",
          makeQueryTemplate(),
        ),
      ].concat(tabs_config);
    }
    defaultTab = resolveDefaultTab(tabs_config, defaultTab);
  } else {
    tabs_config = undefined;
  }

  // the submit callback is responsible for actually executing the query
  const submitCallback = queryCallback;
  const restore = true;

  queryPanel(
    submitCallback,
    tabs_config,
    getSuggestionsCallback,
    restore,
    defaultTab,
  );
}

initQueryPanel();
