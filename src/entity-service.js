import * as EntityServiceModule from "@indiscale/caosdb-webui-entity-service";

/**
 * Expose the entity service to the legacy webui.
 *
 * You can, among other things,
 *
 * 1. create a new TransactionService:
 *    var service = new window.entityService.TransactionService(
 *        window.connection.getBasePath() + "api");
 * 2. execute a query:
 *    response = await service.executeQuery("FIND Something");
 * 3. Use the Entity class to wrapp the "Entity" WebGRPC-Message:
 *    var entity = new windown.entityService.Entity(
 *        response.getResponsesList()[0]
 *                .getRetrieveResponse()
 *                .getFindQueryResult()
 *                .getResultSetList()[0]
 *                .getEntityResponse()
 *                .getEntity());
 */
const entity_service = {

  init: async function () {
    window.entityService = EntityServiceModule;
  },
};

$(document).ready(function () {
  const build = window.BUILD_MODULE_EXT_GRPC_ENTITY_SERVICE || "${BUILD_MODULE_EXT_GRPC_ENTITY_SERVICE}"
  if (build === "ENABLED") {
    caosdb_modules.register(entity_service);
  }
});
