# Release guidelines for the CaosDB WebUI legacy adapter module

This document specifies release guidelines in addition to the general release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* FEATURES.md is up-to-date and a public API is being declared in that document.
* CHANGELOG.md is up-to-date.
* dependencies in `package.json` are up-to-date.

## Steps


1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Update CHANGELOG.md

3. Check all general prerequisites.

4. Update the version:
   * The `version` variable in `package.json`

5. Merge the release branch into the main branch.

6. Tag the latest commit of the main branch with `v<VERSION>`.

7. Delete the release branch.

8. Publish the release by executing `npm publish` which uploads the caosdb
   module to the node package repository [npmjs.com](https://www.npmjs.com).

9. Merge the main branch back into the dev branch.

10. After the merge of main to dev, start a new development version by
    increasing at least the patch version of the `version` in
    [package.json](./package.json) and preparing CHANGELOG.md by adding a new
    "Unreleased" section on top.

11. Create releases on gitlab.com and gitlab.indiscale.com that contain (at
    least) the most recent section of the CHANGELOG as the description and link
    to the NPM module.
