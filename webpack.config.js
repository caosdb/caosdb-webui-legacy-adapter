const path = require('path');
const webpack = require('webpack');

module.exports = {
  devtool: "eval-source-map",
  entry: {
    'query-form' : './src/query-form.js',
    'map' : './src/map.js',
    'grpc-entity-service' : './src/entity-service.js',
    'file-upload': './src/file-upload.js',
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].bundle.js',
    library: {
      name: "CaosDBWebui2",
      type: "window",
    },
    clean: true,
  },
  resolve: {
    modules: [path.join(__dirname, 'src'), 'node_modules'],
    alias: {
      react: path.join(__dirname, 'node_modules', 'react'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        enforce: "pre",
        use: ["source-map-loader"],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({"process": {"env": {"GRPC_API_URI": undefined}}}),
  ],
};

