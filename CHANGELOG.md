# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2024-08-16 ##

### Added ###

* Exposed GRPC Entity Service

### Fixed ###

* Use caosdb-webui-core-components to prevent googlefonts

### Security ###

## [0.2.0] - 2023-09-06 ##

### Added ###

* File Upload component from @indiscale/caosdb-webui-core-components
* Map component @indiscale/caosdb-webui-ext-map

## [0.1.1] - 2023-07-05 ##

### Fixed ###

* Infinite loop in query panel auto-completion.
  [caosdb-webui#225](https://gitlab.com/caosdb/caosdb-webui/-/issues/225)
* Missing `autocomplete="off"` in query form.
  [caosdb-webui#223](https://gitlab.com/caosdb/caosdb-webui/-/issues/223)

## [0.1.0] - 2023-05-30 ##

### Added ###

* Everything, intial release
* Modern query panel with filter options, that is seamlessly integrated into the
  classic CaosDB WebUI.
